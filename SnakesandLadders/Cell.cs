﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakesandLadders
{
    internal class Cell
    {
        public int x, y;
        public string propertyName;

        public Panel backgroundPanel;
        public Panel colorPanel;
        public Color color = new Color();

        internal void Show(Form f)
        {
            backgroundPanel = SetBackgroudPanelProperties();


            Panel propertyNamePanel = SetPropertyNamePanelProperties();
            backgroundPanel.Controls.Add(propertyNamePanel);

            colorPanel = SetColorPanelProperties();
            backgroundPanel.Controls.Add(colorPanel);

            f.Controls.Add(backgroundPanel);
        }

        private Panel SetPropertyNamePanelProperties()
        {
            Panel propertyNamePanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Dock = DockStyle.Top,
                Height = Constants.cellHeight / 2
            };

            Label cellPropertyName = new Label();
            cellPropertyName.Text = propertyName;
            cellPropertyName.Dock = DockStyle.Fill;
            cellPropertyName.TextAlign = ContentAlignment.MiddleCenter;
            cellPropertyName.Font = new Font("Arial", 8);

            propertyNamePanel.Controls.Add(cellPropertyName);

            return propertyNamePanel;
        }

        private Panel SetColorPanelProperties()
        {
            Panel colorPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = color,
                Dock = DockStyle.Top,
                Height = Constants.cellHeight / 5
            };
            return colorPanel;
        }

        private Panel SetBackgroudPanelProperties()
        {
            Panel backgroundPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Left = x,
                Top = y,
                Height = Constants.cellHeight,
                Width = Constants.cellWidth
            };
            return backgroundPanel;
        }
    }
}
