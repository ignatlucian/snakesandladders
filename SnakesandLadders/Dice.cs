﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakesandLadders
{
    internal class Dice
    {
        static Random rand = new Random();

        public static int Roll()
        {
            return rand.Next(1, 7);
        }
    }
}
