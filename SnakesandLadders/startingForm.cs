﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakesandLadders
{
    public partial class startingForm : Form
    {
        SnakesandLaddersGame game;

        public startingForm()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            game = new SnakesandLaddersGame();
            var gF = new gameForm();
            gF.Show();
            //game.Start(this);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
