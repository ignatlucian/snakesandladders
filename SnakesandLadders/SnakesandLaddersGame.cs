﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnakesandLadders
{
    internal class SnakesandLaddersGame
    {
        public Board board = new Board();
        public List<Player> players = new List<Player>();
        public Dice dice = new Dice();
        public gameForm f;
        internal void Start(gameForm f)
        {
            players.Add(new Player { name = "L", position = 0 });
            players.Add(new Player { name = "R", position = 0 });

            board.Show(f);

            this.f = f;

            PlayGame();
        }

        private void PlayGame()
        {
            int currentPlayerIndex = 0;
            Player player = players[currentPlayerIndex];

            while (!Winner(player))
            {
                int dice = Dice.Roll();
                UpdatePlayer(player, dice);
                ShowPlayer(player);

                currentPlayerIndex = NextPlayerIndex(currentPlayerIndex);
                player = players[currentPlayerIndex];

                f.Refresh();
                Thread.Sleep(1000);
            }
        }

        private int NextPlayerIndex(int currentPlayerIndex)
        {
            currentPlayerIndex++;
            return currentPlayerIndex % players.Count;
        }

        private void ShowPlayer(Player player)
        {
            f.ShowPlayer(player);
        }

        private void UpdatePlayer(Player player, int dice)
        {
            player.position += dice;
        }

        private bool Winner(Player player)
        {
            if (player.position == 100)
            {
                return true;
            }
            return false;
        }
    }
}
