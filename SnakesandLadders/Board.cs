﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakesandLadders
{
    class Board
    {
        public List<Cell> cellList = new List<Cell>();
        public void Show(gameForm f)
        {
            foreach (Cell cell in cellList)
            {
                cell.Show(f);
            }
            f.Refresh();
        }
    }
}
