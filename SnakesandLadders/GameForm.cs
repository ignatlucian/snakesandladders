﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakesandLadders
{
    public partial class gameForm : Form
    {
        SnakesandLaddersGame game;

        public gameForm()
        {
            InitializeComponent();
        }

        internal void ShowPlayer(Player player)
        {
            //playerNameLabel.Text = player.name;
            //playerPositionLabel.Text = player.position.ToString();

            game.board.cellList[player.position].
            colorPanel.Controls.Add(new Label { Text = player.name });
        }
    }
}
