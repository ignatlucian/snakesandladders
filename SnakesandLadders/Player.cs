﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakesandLadders
{
    internal class Player
    {
        public string name;
        public int position;

        public Player()
        {
            name = "Default Player";
            position = 1;
        }

        public Player(string name)
        {
            this.name = name;
            position = 1;
        }
    }
}
